# Awsome sustainable software

## What is sustainable software?

Sustainable software is software that is designed and developed with long-term maintainability, efficiency, and usefulness in mind. This means that the software is built with robust and clean code, follows industry best practices, and is designed to be easily understood and modified by future developers.
Additionally, sustainable software is often designed to be energy efficient and have a low impact on the environment, making it a more environmentally friendly choice. By focusing on sustainability, organizations can save money on maintenance and updates in the long run, while also doing their part to protect the environment.

## Resources:

* [Open sustain's curated list of softwares](https://opensustain.tech/)
* [Open source sustainability](https://opensourcesustainability.org/)
