# Awsome sustainable software

## What is sustainable software?

Sustainable software are those software that are designed, developed, deployed, and maintained in a way that minimizes its environmental impact and maximizes its long-term social and economic benefits. Sustainable software is an emerging concept that considers the full life cycle of software development, from its design to its disposal.

## Resources:

[Green-Software-Foundation / awesome-green-software](https://github.com/Green-Software-Foundation/awesome-green-software)
[kube-green / kube-green](https://github.com/kube-green/kube-green)